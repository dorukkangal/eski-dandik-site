package com.eds.view;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class MainWindow extends Window {
	private static final long serialVersionUID = 1L;

	public MainWindow() {
		VerticalLayout rootLayout = new VerticalLayout();
		Label dummy = new Label("<b>Eski Dandik Site</b>", Label.CONTENT_XML);
		rootLayout.addComponent(dummy);
		rootLayout.setSizeFull();
		setContent(rootLayout);
	}
}
