package com.eds.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ScheduleTaskExecutor {

	@Autowired
	private LeftFrameRefresher refresher;

	@Scheduled(fixedDelay = 1000)
	public void executeTask() {
		refresher.refresh();
	}
}
