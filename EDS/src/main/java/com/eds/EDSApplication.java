package com.eds;

import com.eds.i18n.Messages;
import com.eds.session.UserSession;
import com.eds.view.MainWindow;
import com.vaadin.Application;

public class EDSApplication extends Application {
	private static final long serialVersionUID = 1L;

	private MainWindow mainWindow;

	@Override
	public void init() {
		UserSession.setApp(this);
		initBundle();

		mainWindow = new MainWindow();
		setMainWindow(mainWindow);
	}

	private void initBundle() {
		Messages.initDefaultBundle();
	}
}