package com.eds.service;

import org.springframework.stereotype.Service;

import com.eds.domain.User;

@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

	public UserServiceImpl() {
		super(User.class);
	}
}
