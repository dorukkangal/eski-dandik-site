package com.eds.service;

import com.eds.domain.Entry;

public interface EntryService extends BaseService<Entry> {
	public boolean isVisible(Entry entry);
}
