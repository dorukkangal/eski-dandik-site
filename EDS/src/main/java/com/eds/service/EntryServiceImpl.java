package com.eds.service;

import com.eds.domain.Entry;

public class EntryServiceImpl extends BaseServiceImpl<Entry> implements EntryService {

	public EntryServiceImpl() {
		super(Entry.class);
	}

	@Override
	public boolean isVisible(Entry entry) {
		return (!entry.isInTrash() && !entry.isDeleted() && !entry.isDraft() && !entry.getUser().isRookie());
	}
}
