package com.eds.service;

import com.eds.domain.User;

public interface UserService extends BaseService<User> {

}