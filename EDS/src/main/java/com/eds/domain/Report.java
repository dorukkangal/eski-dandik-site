package com.eds.domain;

import javax.persistence.Entity;

@Entity
public class Report extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	private User reporterUser;
	
	private Entry entry;
	
	private ReportReason reason;
	
	private String comments;

	public Entry getEntry() {
		return entry;
	}

	public void setEntry(Entry entry) {
		this.entry = entry;
	}

	public ReportReason getReason() {
		return reason;
	}

	public void setReason(ReportReason reason) {
		this.reason = reason;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public User getReporterUser() {
		return reporterUser;
	}

	public void setReporterUser(User reporterUser) {
		this.reporterUser = reporterUser;
	}

}
