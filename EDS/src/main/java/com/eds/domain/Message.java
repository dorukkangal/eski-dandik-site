package com.eds.domain;

import javax.persistence.Entity;

@Entity
public class Message extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	private User fromUser;
	private User toUser;
	
	private String messageContent;

	public User getFromUser() {
		return fromUser;
	}

	public void setFromUser(User fromUser) {
		this.fromUser = fromUser;
	}

	public User getToUser() {
		return toUser;
	}

	public void setToUser(User toUser) {
		this.toUser = toUser;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

}
