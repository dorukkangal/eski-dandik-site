package com.eds.domain;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;

@Entity
public class User extends BaseEntity {
	private static final long serialVersionUID = 1L;

	
	// Authentication & Personal Info
	private String username;
	private String password;
	
	private String firstName;
	private String lastName;
	
	private String eMailAddress;
	
	private Date birthDate;
	
	/**
	 * Nesil
	 */
	private int descent;
	
	/**
	 * Caylak
	 */
	private boolean isRookie;
	
	/**	 * M or F	 */
	private char gender;
	
	// Options
	private boolean isRemoveEntriesPermanentlyEnabled;
	private boolean isMessagingEnabled;
	private int entryCountInPage;
	private int topicCountInPage;
	
	private boolean isFrozen;
	private Date frozenUntil;
	
	private List<User> buddies;
	private List<User> trolls;
	
	
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String geteMailAddress() {
		return eMailAddress;
	}

	public void seteMailAddress(String eMailAddress) {
		this.eMailAddress = eMailAddress;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public boolean isRemoveEntriesPermanentlyEnabled() {
		return isRemoveEntriesPermanentlyEnabled;
	}

	public void setRemoveEntriesPermanentlyEnabled(
			boolean isRemoveEntriesPermanentlyEnabled) {
		this.isRemoveEntriesPermanentlyEnabled = isRemoveEntriesPermanentlyEnabled;
	}

	public boolean isMessagingEnabled() {
		return isMessagingEnabled;
	}

	public void setMessagingEnabled(boolean isMessagingEnabled) {
		this.isMessagingEnabled = isMessagingEnabled;
	}

	public int getEntryCountInPage() {
		return entryCountInPage;
	}

	public void setEntryCountInPage(int entryCountInPage) {
		this.entryCountInPage = entryCountInPage;
	}

	public int getTopicCountInPage() {
		return topicCountInPage;
	}

	public void setTopicCountInPage(int topicCountInPage) {
		this.topicCountInPage = topicCountInPage;
	}

	public boolean isFrozen() {
		return isFrozen;
	}

	public void setFrozen(boolean isFrozen) {
		this.isFrozen = isFrozen;
	}

	public Date getFrozenUntil() {
		return frozenUntil;
	}

	public void setFrozenUntil(Date frozenUntil) {
		this.frozenUntil = frozenUntil;
	}

	public List<User> getBuddies() {
		return buddies;
	}

	public void setBuddies(List<User> buddies) {
		this.buddies = buddies;
	}

	public List<User> getTrolls() {
		return trolls;
	}

	public void setTrolls(List<User> trolls) {
		this.trolls = trolls;
	}

	public int getDescent() {
		return descent;
	}

	public void setDescent(int descent) {
		this.descent = descent;
	}

	public boolean isRookie() {
		return isRookie;
	}

	public void setRookie(boolean isRookie) {
		this.isRookie = isRookie;
	}
}
