package com.eds.domain;

import javax.persistence.Entity;

@Entity
public class Tag extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	private String tagName;

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

}
