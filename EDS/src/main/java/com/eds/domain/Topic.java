package com.eds.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;


@Entity
public class Topic extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@Column(name = "topic_name",unique=true)
	private String topicName;
	
	private boolean isLocked;
	
	private List<Tag> tags;

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public boolean isLocked() {
		return isLocked;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

}
