package com.eds.domain;

import javax.persistence.Entity;

@Entity
public class Entry extends BaseEntity {
	private static final long serialVersionUID = 1L;

	
	
	private String entryContent;
	
	private int votes;
	
	
	/**
	 * The entry is in the trash box of the User.
	 */
	private boolean isInTrash;
	
	
	/**
	 * Entry is not published, it is a draft. (Kenar'da)
	 */
	private boolean isDraft;
	
	// Relations
	private User user;
	private Topic topic;
	

	public String getEntryContent() {
		return entryContent;
	}

	public void setEntryContent(String entryContent) {
		this.entryContent = entryContent;
	}

	public int getVotes() {
		return votes;
	}

	public void setVotes(int votes) {
		this.votes = votes;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	public boolean isDraft() {
		return isDraft;
	}

	public void setDraft(boolean isDraft) {
		this.isDraft = isDraft;
	}

	public boolean isInTrash() {
		return isInTrash;
	}

	public void setInTrash(boolean isInTrash) {
		this.isInTrash = isInTrash;
	}
	
}
