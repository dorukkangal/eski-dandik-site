package com.eds.domain;

import javax.persistence.Entity;

@Entity
public class ReportReason extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	String reasonText;

	public String getReasonText() {
		return reasonText;
	}

	public void setReasonText(String reasonText) {
		this.reasonText = reasonText;
	}
}
