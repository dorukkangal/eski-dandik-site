package com.eds.domain;

import javax.persistence.Entity;

@Entity
public class EntryHistory extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	private Entry entry;
	private String oldContent;

	public Entry getEntry() {
		return entry;
	}

	public void setEntry(Entry entry) {
		this.entry = entry;
	}

	public String getOldContent() {
		return oldContent;
	}

	public void setOldContent(String oldContent) {
		this.oldContent = oldContent;
	}
}
